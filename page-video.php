<?php
/*
 Template Name: Video Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">
                    

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
                        
                        <div class="video-list">
                        <?php if( have_rows('video_field') ): ?>
                            <section class="videos">
						  <?php while( have_rows('video_field') ): the_row(); ?>                               
                               <h3><?php the_sub_field('section_title'); ?></h3>
                                
                                
                                <?php if( have_rows('video_column') ): ?>
                                <ul class="video-items">
                                  <?php while( have_rows('video_column') ): the_row(); ?>
                                    <li class="items">
                                        <iframe width="300" height="170"
                    src="https://www.youtube.com/embed/<?php echo get_sub_field('youtube_video_id'); ?>" allow="autoplay; encrypted-media" allowfullscreen="true">
                    </iframe>
                                       <h4><?php the_sub_field('video_title'); ?></h4>
                                
                                    </li>
                               
                                    <?php endwhile; ?>
                                </ul>
                                <?php endif; ?>
                               
				            <?php endwhile; ?>
                            </section>
				        <?php endif; ?>
                        </div>                        
					</article>
				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1 class="page-title">Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>