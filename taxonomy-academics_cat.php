<?php get_header(); ?>

			<div class="content main" id="main-content">
				<header>
                    
                    <?php 
                        $cat = get_term_by('name', single_cat_title('', false), 'academics_cat');
                        $current_cat_name = $cat->name;
                        $current_cat_slug = $cat->slug;
                        //echo $current_cat_slug;
                    ?>
                    
                    
					<h1><?php echo $current_cat_name; ?></h1>
                        
                        <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>                   	
				</header>  
				<div class="academics-list">
					<ul <?php post_class('cf'); ?>>
					<?php $core_loop = new WP_Query( array( 'academics_cat' => $current_cat_slug, 'post_type' => 'academics', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC')); ?>
                        
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="academics-item">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'blog-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name">
                                        <?php if(get_field('groups_website')) { ?>
									       <a target="_blank" href="<?php the_field('groups_website'); ?>" ><span class="hidden">Visit the </span><?php the_title(); ?> <span class="hidden">Website (opens in new window)</span></a>
                                        <?php } ?>
                                    </dt>
									<dd class="description">
                                        
                                    <?php if(get_the_term_list( $post->ID, 'academics_cat')){ ?>
                                        <span class="cat-title">
                                            <?php echo get_the_term_list( $post->ID, 'academics_cat', '', ' | ' , ''); ?>                                        
                                        </span>                                       
                                            <br />
                                    <?php } ?>
                                    <?php if(get_field('email_address')) { ?>
                                        <span class="email">
                                            <strong>Email: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a> | 
                                        </span>
                                        <?php } ?>
                                        <?php if(get_field('phone_number')) { ?>
                                        <span class="phone">
                                            <strong>Phone: </strong><?php the_field('phone_number'); ?> | 
                                        </span>
                                        <?php } ?>
									<?php if(get_field('office_hours')) { ?>
									<span class="office_hours">
										<strong>Office Hours: </strong><?php the_field('office_hours'); ?>
									</span>
									<?php } ?>                                        
									<?php if($current_cat_name !== 'Languages') { ?>
                                        <p>
											<?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 90, '...' );
											echo $trimmed_content;
											?>
										</p>
                                    <?php } ?>
                                    </dd>
                                    <?php if(get_the_term_list( $post->ID, 'languages_cat')){ ?>
                                        <dt>Languages offered:</dt>
                                        <dd class="language">                            
                                            <?php
                                                                                             
                                                $product_terms = wp_get_object_terms($post->ID, 'languages_cat');
                                                if ( ! empty( $product_terms ) ) {
                                                    if ( ! is_wp_error( $product_terms ) ) {
                                                        echo '<div>';
                                                            foreach( $product_terms as $term ) {
                                                                echo '<span class="btn">' . esc_html( $term->name ) . '</span>'; 
                                                            }
                                                        echo '</div>';
                                                    }
                                                }
                                            
                                            ?>
                                        </dd>                                    
                                    <?php } ?>
                                    <dd>
                            <?php if(get_field('groups_website')) { ?>
									<a class="btn" target="_blank" href="<?php the_field('groups_website'); ?>" >Visit<span class="hidden"> the <?php the_title(); ?></span> Website<span class="hidden"> (opens in new window)</span></a>
									<?php } ?>
                                    
                                    </dd>
								</dl>
						</li>
					<?php endwhile; ?>			
					</ul>
				</div>
			</div>
<?php get_footer(); ?>