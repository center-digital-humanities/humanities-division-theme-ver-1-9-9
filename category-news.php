<?php get_header(); ?>
			<div class="content main">
                <header>
                <?php if (is_category()) { ?>
					<h1 class="archive-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } elseif (is_tag()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
					</h1>
					<?php } elseif (is_author()) {
						global $post;
						$author_id = $post->post_author;
					?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
					</h1>
					<?php } elseif (is_day()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
					</h1>
					<?php } elseif (is_month()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
					</h1>
					<?php } elseif (is_year()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
					</h1>
					<?php } ?>
                
                </header>
				<div class="col" id="main-content" role="main">
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
						<section class="entry-content cf">
                            <?php if ( has_post_thumbnail() ) { 
                                    $trim_number = 25;
                            ?>
                            <figure class="blog-thumb">                                
				                <?php the_post_thumbnail( 'blog-thumb' ); ?>                            
                            </figure>                         
                            <div class="details">                             
                            <?php }else { 
                                    $trim_number = 50;
                            ?>
                            <div class="details-wide">
                            <?php } ?>
                                <span class="cat-title">
                                    <?php echo get_the_term_list( $post->ID, 'category', '', ' | ' , ''); ?>                                
                                </span>
                                <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                  <!--// span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span //-->
                                    <p>
                                        <?php
                                            $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, $trim_number, '...' );
                                            echo $trimmed_content;
                                        ?>
                                    </p>
                            </div>
						</section>
					</article>

					<?php endwhile; ?>
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>