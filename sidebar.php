				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-label="Section Navigation">
							<?php
								// If a Profiles subpage
								if (is_tree(1305, 108) || get_field('menu_select') == "student" || is_single('person', 'career', 'careers' ) ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Profile', 'bonestheme' ),
										'menu_class' => 'profilee-nav',
										'theme_location' => 'profile-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Student</h3> <ul>%3$s</ul>'
									));
								}
								
								// If an Commencement subpage
								if (is_tree(860, 139) || get_field('menu_select') == "events") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Events', 'bonestheme' ),
									   	'menu_class' => 'events-nav',
									   	'theme_location' => 'events-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Events</h3> <ul>%3$s</ul>'
									));
								}
                    // If an ABOUT subpage
								if (is_tree(101) || get_field('menu_select') == "about-us") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'About Us', 'bonestheme' ),
									   	'menu_class' => 'about-nav',
									   	'theme_location' => 'about-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>About Us</h3> <ul>%3$s</ul>'
									));
								}
                    // If an ABOUT subpage
								if (is_tree(1104) || get_field('menu_select') == "diversity") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Diversity', 'bonestheme' ),
									   	'menu_class' => 'diversity-nav',
									   	'theme_location' => 'diversity-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Diversity</h3> <ul>%3$s</ul>'
									));
								}
								// If a Graduate subpage
								if (is_tree(863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'grad-nav',
										'theme_location' => 'grad-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
					</div>
				</div>
				<?php } ?>