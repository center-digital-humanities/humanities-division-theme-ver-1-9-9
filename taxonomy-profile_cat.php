<?php get_header(); ?>

			<div class="content main">
				<header>                    
                    <?php 
                        $cat = get_term_by('name', single_cat_title('', false), 'profile_cat');
                        $current_cat_name = $cat->name;
                        $current_cat_slug = $cat->slug;
                    ?>                   
                    
					<h1><?php echo $current_cat_name; ?></h1>
					 
				</header>
                
                <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?> 
                <div class="col" id="main-content" role="main">
				<div class="profile-list">
					<ul <?php post_class('cf'); ?>>
					<?php $core_loop = new WP_Query( array( 'profile_cat' => $current_cat_slug, 'post_type' => 'profile', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item">
							<a href="<?php the_permalink() ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'blog-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="description">
                                        <p>
											<?php 
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 20, '...' );
											echo $trimmed_content;
											?>
										</p></dd>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
                                
							</a>
                            <a class="btn" href="<?php the_permalink() ?>">Read More<span class="hidden"> About <?php the_title(); ?></span></a>
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
            </div>

				<div class="col">					
					<div class="content col side">
                        <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Profiles subpage								
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Profile', 'bonestheme' ),
										'menu_class' => 'profilee-nav',
										'theme_location' => 'profile-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Student</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div> 
			</div>
<?php get_footer(); ?>