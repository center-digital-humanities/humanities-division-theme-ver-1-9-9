<?php
/*
	Career Navigation and Hero 
*/
?>

<a href="<?php bloginfo('url'); ?>" class="site-link">Back to <?php bloginfo( 'name' ); ?> Website</a>
<a href="#main-content" class="hidden skip">Skip to main content</a>

<div id="container" class="landing">
<?php

$parent = $post->post_parent;
$grandparents = get_ancestors( $parent, 'page');
//$country = get_field( 'country', $grandparents[0] );
$theparent;
    if ( $grandparents ) {
        //$parent = array_pop( get_post_ancestors($post->ID) );
        $theparent = $grandparents[0];
}
else {
   // $parent = $post->post_parent;
    $theparent = $post->post_parent;
        
}

	if(get_field('series_header_image', $theparent)) {        
            $h_image = get_field('series_header_image', $theparent);
            if( !empty($h_image) ): 
            // vars
            $h_url = $h_image['url'];
            $h_title = $h_image['title'];
            // thumbnail
            $h_size = 'home-hero';
            $h_thumb = $h_image['sizes'][ $h_size ];
            $h_width = $h_image['sizes'][ $h_size . '-width' ];
            $h_height = $h_image['sizes'][ $h_size . '-height' ];  endif;?>
	<div id="hero" style="background-image: url('<?php echo $h_thumb; ?>');">
		<div class="overlay">
		<?php } else { } ?>
			<header role="banner" class="top">
				<nav role="navigation" aria-labelledby="main navigation" class="desktop">
                <?php if ( $post->post_parent > 0 ) {
                        $parent = array_pop( get_post_ancestors($post->ID) );
                    }
                    else {
                        $parent = $post->post_parent;
                    } 
                ?>

                <ul>
                    <li><a href="<?php echo get_permalink($parent); ?>">Home</a></li>
                    <?php
                    if(!$post->post_parent) $my_page_id = $post->ID;
                    elseif($post->ancestors) $my_page_id = end($post->ancestors);

                    $list_p = array(
                     'sort_column' => 'menu_order, post_title',
                    'post_type' => 'careers',
                     'title_li' => '',
                    'depth' => 1,
                     'child_of' => $my_page_id,
                    );
                    wp_list_pages( $list_p );
                    ?>
                </ul>
				</nav>
                
                <?php
                /* THIS WILL DISPLAY TITLE OF PARENT. TO SHOW REMOVE if($something) { } */
                if($something) { ?>
				<div class="content">
                    <?php if ( $post->post_parent ) { ?>	
                        <a href="<?php echo get_permalink($post->post_parent); ?>">
                            <h1>
                                <?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?>
                            </h1>
                        </a>
                    <?php } else { ?>
					<h1 class="<?php if(get_field('title_font_size')) { ?><?php the_field('title_font_size'); ?><?php } ?>">
						<?php if(get_field('pre_title')) { ?><span class="subtitle"><?php the_field('pre_title'); ?></span><?php } ?>
						<?php the_title(); ?></h1>
                    <?php } ?>
                    
                   
        
						
					<?php if(get_field('post_title')) { ?>
					<div class="details">
						<?php the_field('post_title'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('show_button') == "yes") { ?>
					<a href="<?php the_field('button_link'); ?>" class="btn"><?php the_field('button_text'); ?></a>
					<?php } ?>
				</div>
                <?php } ?>
                <div class="logo">                
                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/HCPS_logo.png" alt="Logo" class="photo"/>
                </div>
				<nav role="navigation" aria-labelledby="main navigation" class="mobile">
                    <?php if ( $post->post_parent > 0 ) {
                            $parent = array_pop( get_post_ancestors($post->ID) );
                        }
                        else {
                            $parent = $post->post_parent;
                        } 
                    ?>

                    <ul>
                        <li><a href="<?php echo get_permalink($parent); ?>">Home</a></li>
                        <?php
                        if(!$post->post_parent) $my_page_id = $post->ID;
                        elseif($post->ancestors) $my_page_id = end($post->ancestors);

                        $list_p = array(
                         'sort_column' => 'menu_order, post_title',
                        'post_type' => 'careers',
                         'title_li' => '',
                        'depth' => 1,
                         'child_of' => $my_page_id,
                        );
                        wp_list_pages( $list_p );
                        ?>
                    </ul>
				</nav>
			</header>
        </div>
    </div>
	<?php 
	// Breadcrumbs
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
	}
	?>