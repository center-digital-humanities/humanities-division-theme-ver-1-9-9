<?php
/*
 Template Name: Staff Page
*/
?>
<?php get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
				</header>
                    <?php if($post->post_content!==""): ?>
                        <section>
                            <?php the_content(); ?>
                        </section>
                    <?php endif; ?>
				<div class="profile-list">
				    <?php if( have_rows('staff_profile') ): ?>                                        
					   <ul>
						<?php while( have_rows('staff_profile') ): the_row(); ?>
							<?php
                                $first_name = get_sub_field('first_name');
                                $last_name = get_sub_field('last_name');
								$person_name = $first_name. ' ' . $last_name;
                                $email_address = get_sub_field('email_address');
                                $position_title = get_sub_field('position_title');
                                $phone_number = get_sub_field('phone_number');
                                $office = get_sub_field('office');
                                $office_hours = get_sub_field('office_hours');
								$personal_site = get_sub_field('personal_website');
								$image = get_sub_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'blog-thumb';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif;
							?>
							<li class="person-item">		
                                <?php if( $personal_site ): ?>
                                    <a href="<?php echo $personal_site; ?>" target="_blank">
                                <?php endif; ?>
                                    <?php // if there is a photo, use it
                                    if($image) { ?>
                                    <img src="<?php echo $thumb; ?>" alt="A photo of <?php echo $person_name; ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                    <?php // otherwise use a silhouette 
                                    } else { ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                    <?php } ?>
                                <?php if( $personal_site ): ?>
                                        <span class="hidden">links to the Department Website (opens in new window)</span>
                                    </a>
                                <?php endif; ?>
                                        <dl>
                                            <dt class="name"><?php echo $person_name; ?></dt>
                                            <dd class="position"><?php echo $position_title; ?></dd>
                                            <?php if($email_address) { ?>
                                            <dd class="email">
                                                <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a>
                                            </dd>
                                            <?php } ?>
                                            <?php if($phone_number) { ?>
                                            <dd class="phone">
                                                <?php echo $phone_number; ?>
                                            </dd>
                                            <?php } ?> 
                                            <?php if($office) { ?>
                                            <dd class="office">
                                               <?php echo $office; ?>
                                            </dd>
                                            <?php } ?>
                                            <?php if($office_hours) { ?>
                                            <dd class="office">
                                                <?php echo $office_hours; ?>
                                            </dd>
                                            <?php } ?>
                                        </dl>
                                        <?php if($personal_site) { ?>                                        
                                            <a class="btn" href="<?php echo $personal_site; ?>" target="_blank"><span class="hidden">Visit <?php echo $person_name; ?> Department </span>Website <span class="hidden"> (opens in new window)</span></a>
                                        <?php } ?>
							</li>
							<?php endwhile; ?>
                            </ul>
				        <?php endif; ?>
                                
					</div>
			</div>
<?php get_footer(); ?>