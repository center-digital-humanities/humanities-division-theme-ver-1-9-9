<?php
/*
	Basic Navigation and Hero 
*/
?>
<a href="#main-content" class="hidden skip" tabindex="1">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
            <?php if ( is_front_page() ) { ?><h1 class="hidden"><?php the_field('department_name', 'option'); ?></h1><?php }?>
			<a href="<?php echo home_url(); ?>" class="university-logo" rel="nofollow">
                <img src="<?php echo get_template_directory_uri(); ?>/library/images/Uxd_Wht_Humanities_B.svg" alt="<?php the_field('department_name', 'option'); ?> logo" class="dept-logo" />
            </a>
			<?php if(get_field('enable_donation', 'option') == "enable") { ?>
			<div class="give-back">
				<?php if(get_field('link_type', 'option') == "internal") { ?>
				<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
				<?php }?>
				<?php if(get_field('link_type', 'option') == "external") { ?>
				<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
				<?php }?><span class="fas fa-heart" aria-hidden="true"></span>
				<?php the_field('button_text', 'option'); ?></a>
				<?php if(get_field('supporting_text', 'option')) { ?>
				<span class="support"><?php the_field('supporting_text', 'option'); ?></span>
				<?php }?>
			</div>
			<?php }?>
            <nav role="navigation" aria-label="Main Navigation" class="desktop">
                <?php wp_nav_menu(array(
                    'container' => false,
                    'menu' => __( 'Main Menu', 'bonestheme' ),
                    'menu_class' => 'main-nav',
                    'theme_location' => 'main-nav',
                    'before' => '',
                    'after' => '',
                    'depth' => 2,
                )); ?>
                <?php get_search_form(); ?>
            </nav>
		</div>
	</header>
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category( $category ) || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>