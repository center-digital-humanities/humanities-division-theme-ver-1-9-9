<?php get_header(); ?>
<div class="content">
   <?php if(get_field('page')){ }else{ ?> <h1><?php if(get_field('make_featured_icons', 'option') == "icon_display") { the_post_thumbnail( 'speaker-photo' ); } ?><?php the_title(); ?></h1> <?php } ?>
    <div <?php if(get_field('page')){ }else{ ?>  class="col" <?php } ?> id="main-content" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

							<?php if(get_field('make_featured_icons', 'option') == "full_display") { the_post_thumbnail( 'content-width' ); } ?>
            <section class="entry-content cf" itemprop="articleBody">

                <?php $i = 0;
                if(get_field('page')){                    
                    get_template_part('snippets/cps', 'parent');
                    }
                    // This is to display Single Page Events
                    elseif(get_field('page_type') == "about") {                                                         
                       get_template_part('snippets/cps', 'about');
                    }
                    // This is to display Single Page Events
                    elseif(get_field('page_type') == "events") {                                                         
                       get_template_part('snippets/cps', 'events');
                    }
                    // This is to display Single Page Events
                    elseif((get_field('page_type') == "regular-po") || (get_field('page_type') == "resources")) {
                       get_template_part('snippets/cps', 'resources');
                    }
                    /* THIS WILL DISPLAY RESOURCES BLOG */
                    elseif(get_field('page_type') == "resources-blog") {                                                             
                       get_template_part('snippets/cps', 'resourcesblog');
                    }
                    /* THIS WILL DISPLAY EVENTS BLOG */
                   elseif(get_field('page_type') == "event-blog") {                                                             
                       get_template_part('snippets/cps', 'eventsblog');
                   }
                    // This is to display Single Page Events
                    else {
                       get_template_part('snippets/cps', 'resources');
                    } ?>                

            </section>
        </article>

    <?php endwhile; else : ?>

        <article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
            <h1>Page Not Found</h1>
            <section>
                <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
            </section>
        </article>

    <?php endif; ?>

    </div>
    <?php if(get_field('page')){  ?>
    
    <?php } else { ?>
    <div class="col">					
        <div class="content col side">
            <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
                <?php if((get_field('page_type') == "events") || (get_field('page_type') == "event-blog")){ ?>
                <h3>Events</h3>
                <ul>
                    <li><a href="../events/">Upcoming Events</a></li>
                    <li><a href="../past-events/">Past Events</a></li>
                </ul>                
                <? } ?>
                <?php if((get_field('page_type') == "no") || (get_field('page_type') == "resources-blog")){ ?>
                <h3>Resources</h3>
                    <ul>
                        <?php 
                        $terms = get_field( 'resources_filters');
                        foreach( $terms as $term ):?>
                            <li><a href="<?php echo get_term_link( $term );?>"> <?php echo $term->name; ?> </a></li>
                    <?php endforeach; ?>
                    </ul>                
                <?php } ?>
                <?php if((get_field('page_type') == "about")){ ?>
                <h3>About Us</h3>
                <ul>
                    <li><a href="#mission">Mission Statement</a></li>
                    <li><a href="#committee">Committee</a></li>
                </ul>
                
                <? } ?>
            </nav>  
        </div>
    </div>
    <?php } ?>
</div>

<?php get_footer(); ?>
