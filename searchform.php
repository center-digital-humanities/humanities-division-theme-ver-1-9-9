<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label class="hidden" for="search-header">Search</label>
	<input type="search" class="search" id="search-header"placeholder="<?php echo esc_attr_x( 'Search…', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" autocomplete="off" />
    <button type="submit" class="search-btn" aria-label="Submit Search"><span class="fas fa-search"></span></button>
</form>