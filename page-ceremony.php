<?php
/*
 Template Name: Commencement Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">
                    

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
                    
                        <?php $page = get_field('page');
                            $page_query = new WP_Query( array( 'page_id' => $page ) );
                            if ($page_query->have_posts()) : while ($page_query->have_posts()) : $page_query->the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">                      
                                <div class="video-list">
                                <?php if( have_rows('video_field') ): ?>
                                    <section class="videos">
                                  <?php while( have_rows('video_field') ): the_row(); 

                                        $i++;
                                        if( $i > 1 ):
                                            break; 
                                        endif;

                                        ?>                               
                                       <h2 class="page-title"><?php the_sub_field('section_title'); ?></h2>
                                        
                                        <h3>Videos</h3>  


                                        <?php if( have_rows('video_column') ): ?>
                                        <ul class="video-items">
                                          <?php while( have_rows('video_column') ): the_row(); ?>
                                            <li class="items">
                                                <iframe width="300" height="170"
                            src="https://www.youtube.com/embed/<?php echo get_sub_field('youtube_video_id'); ?>" allow="autoplay; encrypted-media" allowfullscreen="true">
                            </iframe>
                                               <h4><?php the_sub_field('video_title'); ?></h4>

                                            </li>

                                            <?php endwhile; ?>
                                        </ul>
                                        <?php endif; ?>

                                    <?php endwhile; ?>
                                    </section>
                                <?php endif; ?>
                                </div>                        
                                <a class="btn" href="<?php the_permalink() ?>">View All<span class="hidden"> <?php the_title(); ?></span></a>
                            </article>
                        <?php endwhile; endif; ?>
                        <?php wp_reset_postdata(); ?>
                            <?php 

                            $images = get_field('add_images');
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)

                            if( $images ): ?>
                        
                        <div id="gallery-2" class="gallery galleryid-10581 gallery-columns-3 gallery-size-thumbnail">
                            <h3 class="page-title">Photos</h3>  
                                    <?php foreach( $images as $image ): ?>
                                        <dl class="gallery-item">
                                            <dt class="gallery-icon landscape">
                                                <a href="<?php echo $image['url']; ?>" rel="lightbox[gallery-0]" data-lightbox-gallery="lightbox[gallery-0]" target="_blank">
                                                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                </a>
                                                <p><?php echo $image['caption']; ?></p>                                        
                                            </dt>
                                        </dl>
                                    <?php endforeach; ?>
                            <?php endif; ?>
                            
                            <?php if(get_field('gallery_link')){ ?>
                                <div>
                                    <a href="<?php echo get_field('gallery_link'); ?>" class="btn">View All</a>
                                </div>
                        
                        </div>
                            <?php } ?>
                        
					</article>

				<?php endwhile; else : ?>
                    
                   

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1 class="page-title">Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>