<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js ie lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js ie"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<!--<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#cc3333">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
	</head>
	<body <?php body_class(); ?>>	

<a href="<?php bloginfo('url'); ?>" class="site-link">Back to <?php bloginfo( 'name' ); ?> Website</a>
<a href="#main-content" class="hidden skip">Skip to main content</a>

<div id="container" class="landing">
<?php

$parent = $post->post_parent;
$grandparents = get_ancestors( $parent, 'page');
//$country = get_field( 'country', $grandparents[0] );
$theparent;
    if ( $grandparents ) {
        //$parent = array_pop( get_post_ancestors($post->ID) );
        $theparent = $grandparents[0];
}
else {
   // $parent = $post->post_parent;
    $theparent = $post->post_parent;
    echo $theparent;
        
}

	if(get_field('series_header_image', $theparent)) {        
            $h_image = get_field('series_header_image', $theparent);
            if( !empty($h_image) ): 
            // vars
            $h_url = $h_image['url'];
            $h_title = $h_image['title'];
            // thumbnail
            $h_size = 'home-hero';
            $h_thumb = $h_image['sizes'][ $h_size ];
            $h_width = $h_image['sizes'][ $h_size . '-width' ];
            $h_height = $h_image['sizes'][ $h_size . '-height' ];  endif;?>
	<div id="hero" style="background-image: url('<?php echo $h_thumb; ?>');">
		<div class="overlay">
		<?php } else { } ?>
			<header role="banner" class="top">
				<nav role="navigation" aria-labelledby="main navigation" class="desktop">
                <?php if ( $post->post_parent > 0 ) {
                        $parent = array_pop( get_post_ancestors($post->ID) );
                    }
                    else {
                        $parent = $post->post_parent;
                    } 
                ?>

                <ul>
                    <li><a href="<?php echo get_permalink($parent); ?>">Home</a></li>
                    <?php
                    if(!$post->post_parent) $my_page_id = $post->ID;
                    elseif($post->ancestors) $my_page_id = end($post->ancestors);

                    $list_p = array(
                     'sort_column' => 'menu_order, post_title',
                    'post_type' => 'careers',
                     'title_li' => '',
                    'depth' => 1,
                     'child_of' => $my_page_id,
                    );
                    wp_list_pages( $list_p );
                    ?>
                </ul>
				</nav>
                
                <?php
                /* THIS WILL DISPLAY TITLE OF PARENT. TO SHOW REMOVE if($something) { } */
                if($something) { ?>
				<div class="content">
                    <?php if ( $post->post_parent ) { ?>	
                        <a href="<?php echo get_permalink($post->post_parent); ?>">
                            <h1>
                                <?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?>
                            </h1>
                        </a>
                    <?php } else { ?>
					<h1 class="<?php if(get_field('title_font_size')) { ?><?php the_field('title_font_size'); ?><?php } ?>">
						<?php if(get_field('pre_title')) { ?><span class="subtitle"><?php the_field('pre_title'); ?></span><?php } ?>
						<?php the_title(); ?></h1>
                    <?php } ?>
                    
                   
        
						
					<?php if(get_field('post_title')) { ?>
					<div class="details">
						<?php the_field('post_title'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('show_button') == "yes") { ?>
					<a href="<?php the_field('button_link'); ?>" class="btn"><?php the_field('button_text'); ?></a>
					<?php } ?>
				</div>
                <?php } ?>
				<nav role="navigation" aria-labelledby="main navigation" class="mobile">
                    <?php if ( $post->post_parent > 0 ) {
                            $parent = array_pop( get_post_ancestors($post->ID) );
                        }
                        else {
                            $parent = $post->post_parent;
                        } 
                    ?>

                    <ul>
                        <li><a href="<?php echo get_permalink($parent); ?>">Home</a></li>
                        <?php
                        if(!$post->post_parent) $my_page_id = $post->ID;
                        elseif($post->ancestors) $my_page_id = end($post->ancestors);

                        $list_p = array(
                         'sort_column' => 'menu_order, post_title',
                        'post_type' => 'careers',
                         'title_li' => '',
                        'depth' => 1,
                         'child_of' => $my_page_id,
                        );
                        wp_list_pages( $list_p );
                        ?>
				</nav>
        <div class="logo">                
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/HCPS_logo.png" alt="Logo" class="photo"/>
        </div>
			</header>
	<?php 
	// Breadcrumbs
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
	}
	?>
                <!--?php get_header(); ?-->
                
                
                
                
                
                
                <?php
                    //$term = get_sub_field('category');
                    //$amount = get_sub_field('amount_to_show');
                    $term = 'interviews';
                    $amount = 10;

                // find date time now
                $date_now = date('Y-m-d H:i:s');
                $time_now = strtotime($date_now);


                // find date time in 7 days
                $time_next_week = strtotime('+12 month', $time_now);
                $date_next_week = date('Y-m-d H:i:s', $time_next_week);

                $slug = get_queried_object()->post_name;
                //echo $slug;

                if(($slug == 'past-events') || ($slug == 'old-stuff') || ($slug == 'pevents')){

                //Return posts from January 1st to February 28th  Source (https://codex.wordpress.org/Class_Reference/WP_Query)
                //PAST EVENTS

                    $past = 'January 1st, 2010';

                    $args = array(
                    'posts_per_page'	=> -1,
                    //'careers_cat' => 'events, hcps', 
                    'post_type' => 'careers', 
                    'meta_query' 		=> array(
                        array(
                            'key'			=> 'date_of_event',
                            'compare'		=> 'BETWEEN',
                            'value'			=> array( $past, $date_now ),
                            'type'			=> 'DATETIME'
                        )
                    ),
                    'order'				=> 'DESC',
                    'orderby'			=> 'meta_value',
                    'meta_key'			=> 'date_of_event',
                    'meta_type'			=> 'DATETIME'
                );

                }else{

                // Upcoming Events Query (https://www.advancedcustomfields.com/resources/date-time-picker/)
                // query events
                $args = array(
                    'posts_per_page'	=> -1,
                       // 'careers_cat' => 'events, hcps, career-panel-series, workshops', 
                        'post_type' => 'careers', 
                    'meta_query' 		=> array(
                        array(
                            'key'			=> 'date_of_event',
                            'compare'		=> 'BETWEEN',
                            'value'			=> array( $date_now, $date_next_week ),
                            'type'			=> 'DATETIME'
                        )
                    ),
                    'order'				=> 'ASC',
                    'orderby'			=> 'meta_value',
                    'meta_key'			=> 'date_of_event',
                    'meta_type'			=> 'DATETIME'
                );


                }
                   $posts_query = new WP_Query( $args );

                   // 'date_query' => array( array( 'after' => '3 days ago' ))

                ?>



			<div class="content main" id="main-content">
				<header>
                    
                    <?php 
                        $cat = get_term_by('name', single_cat_title('', false), 'careers_cat');
                        $current_cat_name = $cat->name;
                        $current_cat_slug = $cat->slug;
                        //echo $current_cat_slug;
                    ?> 
					<h1><?php echo $current_cat_name; ?></h1>
                        
                        <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>                  	
				</header>
				<div class="col" id="main-content" role="main">                               
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
						<section class="entry-content cf">
                            <?php if ( has_post_thumbnail() ) { 
                                    $trim_number = 25;
                            ?>
                            <figure class="blog-thumb">                                
				                <?php the_post_thumbnail( 'blog-thumb' ); ?>                            
                            </figure>                         
                            <div class="details">                             
                            <?php }else { 
                                    $trim_number = 50;
                            ?>
                            <div class="details-wide">
                            <?php } ?>
                                <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                    <dl>
                                        <?php if(get_field('first_name')) { ?>
                                            <dt class="name">
                                                <h3><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></h3>
                                            </dt>
                                        <?php } ?>
                                        <?php if(get_field('date_of_event')) { ?>
                                        <span class="date">
                                            <strong>Date: </strong><?php the_field('date_of_event'); ?> | 
                                        </span>
                                        <?php } ?>
                                        <?php if(get_field('time_of_event')) { ?>
                                        <span class="time">
                                            <strong>Time: </strong><?php the_field('time_of_event'); ?> | 
                                        </span>
                                        <?php } ?>
                                        <?php if(get_field('location')) { ?>
                                        <span class="location">
                                            <strong>Location: </strong><?php the_field('location'); ?>
                                        </span>
                                        <?php } ?>
                                
                                    </dl>
									
                                  <!--// span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span //-->
                                    <p>
                                        <?php
                                            $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, $trim_number, '...' );
                                            echo $trimmed_content;
                                        ?>
                                    </p>
                                <a href="<?php the_permalink() ?>" class="btn">Read More</a>
                            </div>
						</section>
					</article>

					<?php endwhile; ?>
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>
                </div>                    
                <div class="col">					
                    <div class="content col side">
                        <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
                            <?php if((get_field('page_type') == "events") || (get_field('page_type') == "event-blog")){ ?>
                            <h3>Events</h3>
                            <ul>
                                <li><a href="../events/">Upcoming Events</a></li>
                                <li><a href="../past-events/">Past Events</a></li>
                            </ul>                
                            <? }else{ ?>
                            <?php if(get_field('resources_filters', 6415)){  ?>
                            <h3>Resources</h3>
                                <ul>
                                    <?php 
                                    $terms = get_field( 'resources_filters', 6415);
                                    foreach( $terms as $term ):?>
                                        <li><a href="<?php echo get_term_link( $term );?>"> <?php echo $term->name; ?> </a></li>
                                <?php endforeach; ?>
                                </ul>                
                            <?php } } ?>                            
                        </nav>  
                    </div>
                </div>
			</div>

<?php get_footer(); ?>