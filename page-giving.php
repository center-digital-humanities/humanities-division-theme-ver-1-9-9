<?php
/*
 Template Name: Giving Page
*/
?>
<?php get_header(); ?>
			<div class="container main" id="main-content">
                
                
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    
					<h1 class="giving-title"><?php the_title(); ?><div class="arrow-down"></div></h1>
				<div class="give-hero">
                    
                    <?php if(get_field('dean_photo')) {
                            $dp_image = get_field('dean_photo');
                            if( !empty($dp_image) ): 
                                // vars
                                $url = $dp_image['url'];
                                $title = $dp_image['title'];
                                // thumbnail
                                $size = 'home-hero';
                                $dp_thumb = $dp_image['sizes'][ $size ];
                                $width = $dp_image['sizes'][ $size . '-width' ];
                                $height = $dp_image['sizes'][ $size . '-height' ];
                            endif; ?>
                    
                    
                <div id="hero" class="dean_msg" style="background-image: url('<?php echo $dp_thumb; ?>');">
                    
                        
                    <div class="content main" id="main-content">
                            <!--// img src="<?php echo $dp_thumb; ?>" alt="<?php the_title(); ?> dean photo" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" / //-->
                            <?php } else { ?>
                                <div class="custom-cover cover">
                                    <span class="title"><?php the_title(); ?></span>
                                </div>
                            <?php } ?> 
                        <div class="details">
                            <?php the_field('dean_message'); ?>
                        
                        </div>
                        
                
                    
                </div>
                    </div>
                <div class="give_info ">
                    <div class="content main" id="main-content">
                    <?php the_field('giving_info'); ?>
                    </div>
                </div>
                    
                  <?php the_content(); // displays whatever you wrote in the wordpress editor ?>
                  <?php endwhile; endif; //ends the loop ?>
                    
                    </div>
</div>
                <h2 class="giving-title">Support our Departments and Centers
<div class="arrow-down"></div></h2>
                
                <div class="content main" id="main-content">
                <?php if( have_rows('support_department') ): ?> 
				<div class="giving-list content">				                                           
					   <ul>
						<?php while( have_rows('support_department') ): the_row(); ?>
							<?php
                                $department_title = get_sub_field('department_title');
                                $brief_summary = get_sub_field('brief_summary');
								$give_url = get_sub_field('give_url');
								$image = get_sub_field('dc_photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'blog-thumb';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $give_url ): ?>
							<a href="<?php echo $give_url; ?>" class="hero-link">
							<?php endif; ?>
							<li class="person-item">
                                <?php if($thumb): ?>
                                    <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                <?php endif; ?>
									<dl>
                                        <dt class="title"><h4><?php echo $department_title; ?></h4></dt>
                                        <?php if($brief_summary) { ?>
                                        <dd class="summary">
                                            <?php echo $brief_summary; ?>
                                        </dd>
                                        <?php } ?>
                                        <?php if($give_url) { ?>
                                        <dd class="button">
                                            <a class="btn" href="<?php echo $give_url; ?>">Give</a>
                                        </dd>
                                        <?php } ?>
								    </dl>
							</li>
							<?php if( $give_url ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
                            </ul>                                
					   </div>
				        <?php else: ?>              
                
                
				<div class="giving-list content">
					<ul <?php post_class('cf'); ?>>
                        <?php $core_loop = new WP_Query( array( 'academics_cat' => array('departments', 'centers', 'languages'), 'post_type' => 'academics', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC')); ?>
                        
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'blog-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
								</dl>
                            <?php if(get_field('give_url')) { ?>
				                <a class="btn" href="<?php the_field('give_url'); ?>">Give Back</a>
				            <?php } ?>
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
                
                <?php endif; ?>
			</div>
<?php get_footer(); ?>