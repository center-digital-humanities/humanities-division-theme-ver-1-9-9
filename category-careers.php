<?php
/**
 * Template Name: Career Category Index Page
 */
    get_header(); ?>
			<div class="content main">
                <header>
                
				    <h1><?php the_title(); ?></h1>
                
                </header>
				<div class="col" id="main-content" role="main">
                    <?php the_content(); ?>
					<?php $core_loop = new WP_Query( array( 'post_type' => 'careers', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC')); ?>
                        
					<?php if ($core_loop->have_posts()) : while ($core_loop->have_posts()) : $core_loop->the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
						<section class="entry-content cf">
                            <figure class="blog-thumb">                                
							<?php the_post_thumbnail( 'blog-thumb' ); ?>                            
                            </figure>
                            <div class="details">
                                <span class="cat-title">
                                    <?php echo get_the_term_list( $post->ID, 'category', '', ', ' , ''); ?>                                
                                </span>
                                <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                  <span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
                                    <p>
                                        <?php
                                            $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, 23, '...' );
                                            echo $trimmed_content;
                                        ?>
                                    </p>
                                <a href="<?php the_permalink() ?>" class="btn">Read More<span class="hidden"> About <?php the_title(); ?></span></a>
                            </div>
						</section>
					</article>

					<?php endwhile; ?>
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>

				<div class="col">					
					<div class="content col side">
                        <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Profiles subpage								
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Profile', 'bonestheme' ),
										'menu_class' => 'profilee-nav',
										'theme_location' => 'profile-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Student</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div> 
			</div>

<?php get_footer(); ?>