<?php
/*
 Template Name: Statistics Template
*/
?>
<?php get_header(); ?>
        
<div class="container">
    <div class="main" id="main-content" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="content">            
                <h1><?php the_title(); ?></h1>
                <section>
                    <?php the_content(); ?>
                </section>
            </div>
            <div class="stats-container">
					<?php
                    // Stat Row
					if(have_rows('stats')) :
						while (have_rows('stats')) : the_row(); ?>
                
                    
                    
                        <div class="content-col <?php the_sub_field('highlight_section'); the_sub_field('content_width'); ?>">
                            <div class="content">
                            <h3><?php the_sub_field('title');?></h3>
                            <?php 

                                $mycount = count(get_sub_field('content_column'));
                                // Content column
                                $set_col = 0;

                                if ($mycount == 3){
                                        $set_col = 'three_col';
                                    }elseif($mycount == 2){
                                        $set_col = 'two_col';
                                    }else {
                                        $set_col = 'one_col';
                                    }

                                if(have_rows('content_column')) :
                                    while (have_rows('content_column')) : the_row(); ?>
                            <?php 
                                // Element Group
                                if(have_rows('element_group')) :
                                    while (have_rows('element_group')) : the_row();         
                                ?>
                                <div class="stat_col <?php echo $set_col; ?> <?php if(get_sub_field('brief')){ echo 'brief'; } ?>">
                                <?php if(get_sub_field('icons_btn') !== 'none') : ?>
								    <img src="<?php echo get_template_directory_uri(); ?>/library/images/<?php the_sub_field('icons_btn'); ?>.png" alt="A photo of <?php the_sub_field('icons_btn'); ?>" class="icons"/>
                                <?php endif; ?>
                                    <h1><?php the_sub_field('numbers'); ?></h1>                                    
                                    <h5><?php the_sub_field('title'); ?></h5>
                                    <p><?php the_sub_field('brief'); ?></p>
                                    <?php if(get_sub_field('button_link') ) { ?>
                                        <a class="btn" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_title'); ?></a>
                                    <?php } ?>
                            </div> 
                            
                            <?php
                                    endwhile;
                                endif;
                            ?>
                            
                            <?php
                                    endwhile;
                                endif;
                            ?>
                            </div>
                        </div>
                
                    <?php if(get_sub_field('asterisk_row')){; ?>
                        <div class="asterisk">
                            <?php echo get_sub_field('asterisk_row'); ?> 
                        </div>               
                    <?php } ?>
                    <?php
							
						endwhile;
					endif;
			    	?>
				</div>	
        </article>

    <?php endwhile; else : ?>

        <article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
            <h1>Page Not Found</h1>
            <section>
                <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
            </section>
        </article>

    <?php endif; ?>

    </div>
</div>

<?php get_footer(); ?>