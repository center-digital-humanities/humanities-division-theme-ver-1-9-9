<?php
/**
 * Template Name: Profile Category Index Page
 */
    get_header(); ?>

			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
					<?php if ( has_nav_menu( 'faculty-filter' ) ) {?> 
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<button data-filter="" class="option all is-checked">View All</button>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Faculty Filter', 'bonestheme' ),
									'menu_class' => 'faculty-filter',
									'theme_location' => 'faculty-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</ul>
                            
                            
						</div>
                        <div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<button data-filter="" class="option all is-checked">View All</button>
								<button data-filter=".faculty" class="option department">Faculty</button>
<button data-filter=".graduate">Graduate</button>
<button data-filter=".undergraduate">Undergraduate</button>
							</ul>
						</div>
                        
					</div>
					<?php if( have_rows('filters', 'option') ): ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$('.all').click(function() {
								$('.filter-title').html('All');
							});
							<?php while( have_rows('filters', 'option') ): the_row();
							// vars
								$class = get_sub_field('class');
								$category = get_sub_field('category');
							?>
							$('.filter .<?php echo $class ?>').click(function() {
								$('.filter-title').html('<?php echo $category->name; ?>');
							});						
							<?php endwhile; ?>
						});
					</script>
					<?php endif; ?>					
					<h2 class="filter-title">All</h2>
					<?php } ?> 
				</header>
				<div class="profile-list">
					<ul <?php post_class('cf'); ?>>
					<?php $core_loop = new WP_Query( array( 'profile_cat' => array('student', 'graduate', 'undergraduate','faculty'), 'post_type' => 'profile', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
                        <?php
                            $terms = get_the_terms( $post->ID, 'profile_cat' );

                            if ( $terms && ! is_wp_error( $terms ) ) { 

                                $draught_links = array();

                                foreach ( $terms as $term ) {
                                    $draught_links[] = $term->slug;
                                }
                                }

                                $on_draught = join( " ", $draught_links );
                        ?>
						<li data-category="transition" class="person-item <?php echo $on_draught; ?> ">
							<a href="<?php the_permalink() ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'blog-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
                                        <span class="cat-title">
                                            <?php echo get_the_term_list( $post->ID, 'profile_cat', '', ',' , ''); ?>                                        
                                        </span>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="description">
                                        <p>
											<?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 15, '...' );
											echo $trimmed_content;
											?>
										</p></dd>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
                                
							</a>
                            <a class="btn" href="<?php the_permalink() ?>">Read More</a>
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
			</div>
<?php get_footer(); ?>