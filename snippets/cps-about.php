<section id="mission" class="thecontent">
    <h2>Mission Statement</h2>
    <?php the_content(); ?>
</section>
<section id="committee">        
    <?php
        // For displaying snippet to show Speakers list                              
        if( have_rows('members') ) {   
            echo '<h3>Meet The Committee</h3>';?>
    
				<div class="people-list members">
					<ul <?php post_class('cf'); ?>>
            
            <?php while ( have_rows('members') ) { the_row();
                                                              
                $member_name = get_sub_field('member_name');
                $member_title = get_sub_field('member_title');
                $member_department = get_sub_field('member_department'); 
            ?>
					
						<li class="person-item">
							
								<?php // if there is a photo, use it
                                    if(get_sub_field('member_photo')) {
                                    $image_cpa = get_sub_field('member_photo');
                                        
									if( !empty($image_cpa) ): 
										// vars
										$url = $image_cpa['url'];
										$title = $image_cpa['title'];
										// thumbnail
										$size = 'speaker-photo';
										$thumb_cp = $image_cpa['sizes'][ $size ];
										$width = $image_cpa['sizes'][ $size . '-width' ];
										$height = $image_cpa['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb_cp; ?>" alt="A photo of <?php echo $title; ?>" class="photo wp-post-image <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
                                    <?php if ($member_name){  ?>
                                        <dt class="name">            
                                            <?php echo $member_name; ?>
                                        </dt>
                                    <?php } ?>                                    
                                    <?php if ($member_title){ ?>
                                        <dd class="member_title position">
                                            <?php echo $member_title; ?>
                                        </dd>
                                    <?php } ?>
                                    <?php if ($member_department){ ?>
                                        <dd class="member_department interest">
                                            <?php echo $member_department; ?>
                                        </dd>
                                    <?php } ?>
                                    
								</dl>
						</li>
    <?php }} ?>				
					</ul>
				</div>
    </section>