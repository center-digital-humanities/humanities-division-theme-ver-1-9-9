<div class="schedule">
	<h5><?php the_sub_field('day'); ?></h5>
	
	<?php if( have_rows('sessions') ): ?>
		<ul>
		<?php while( have_rows('sessions') ): the_row(); ?>
			<li>
				<div class="time">
					<?php the_sub_field('time'); ?>
				</div>
				<div class="session">
					<h6><?php the_sub_field('title'); ?></h6>
					<?php the_sub_field('description'); ?>
				</div>
			</li>
		<?php endwhile; ?>
		</ul>
	
	<?php endif; ?>
</div>