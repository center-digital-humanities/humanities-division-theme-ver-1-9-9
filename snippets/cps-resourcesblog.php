<?php get_search_form(); ?> 
<?php
        //$term = get_sub_field('category');
        //$amount = get_sub_field('amount_to_show');
        $term = 'interviews';
        $amount = 10;

       $posts_query = new WP_Query( array( 'careers_cat' => 'resources', 'post_type' => 'careers', 'orderby' => 'menu_order', 'order' => 'ASC') ); 

    ?>
    <div class="archive resources">
            <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>

            <?php if($description) { ?>
                <?php echo $description; ?>
                <hr />

        <?php } else { ?>                        
                <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>
            <?php } ?>                  

            <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
                <section class="entry-content cf">
                    <div class="details-wide">
                        <h4 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                        <?php if(get_the_term_list( $post->ID, 'careers_cat')){ ?>
                            <span class="cat-title">
                                <?php echo get_the_term_list( $post->ID, 'careers_cat', '', ' | ' , ''); ?>                                        
                            </span>
                        <?php } ?>
                        <p>
                            <?php
                                $content = get_the_content();
                                $trimmed_content = wp_trim_words( $content, 50, '...' );
                                echo $trimmed_content;
                            ?>
                        </p>
                        <a href="<?php the_permalink() ?>" class="btn">More Details</a>
                    </div>
                </section>
            </article>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </div>