<?php
    //$term = get_sub_field('category');
    //$amount = get_sub_field('amount_to_show');
    $term = 'interviews';
    $amount = 10;

// find date time now
$date_now = date('Y-m-d H:i:s');
$yesterday = date('Y-m-d H:i:s', strtotime("-1 day"));
$time_now = strtotime($date_now);

// find date time in 7 days
$time_next_week = strtotime('+12 month', $time_now);
$date_next_week = date('Y-m-d H:i:s', $time_next_week);

$slug = get_queried_object()->post_name;
//echo $slug;

if(($slug == 'past-events') || ($slug == 'old-stuff') || ($slug == 'pevents')){

//Return posts from January 1st to February 28th  Source (https://codex.wordpress.org/Class_Reference/WP_Query)
//PAST EVENTS
    
    $past = 'January 1st, 2010';
    
    $args = array(
	'posts_per_page'	=> -1,
    'careers_cat' => 'events, hcps', 
    'post_type' => 'careers', 
	'meta_query' 		=> array(
		array(
	        'key'			=> 'date_of_event',
	        'compare'		=> 'BETWEEN',
	        'value'			=> array( $past, $yesterday ),
	        'type'			=> 'DATETIME'
	    )
    ),
	'order'				=> 'DESC',
	'orderby'			=> 'meta_value',
	'meta_key'			=> 'date_of_event',
	'meta_type'			=> 'DATETIME'
);
    
}else{

// Upcoming Events Query (https://www.advancedcustomfields.com/resources/date-time-picker/)
// query events
$args = array(
	'posts_per_page'	=> -1,
        'careers_cat' => 'events, hcps, career-panel-series, workshops', 
        'post_type' => 'careers', 
	'meta_query' 		=> array(
		array(
	        'key'			=> 'date_of_event',
	        'compare'		=> 'BETWEEN',
	        'value'			=> array( $yesterday, $date_next_week ),
	        'type'			=> 'DATETIME'
	    )
    ),
	'order'				=> 'ASC',
	'orderby'			=> 'meta_value',
	'meta_key'			=> 'date_of_event',
	'meta_type'			=> 'DATETIME'
);
    
    
}
   $posts_query = new WP_Query( $args );

   // 'date_query' => array( array( 'after' => '3 days ago' ))

?>
<div class="archive events">
        <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>

                <?php // if there is a photo, use it
                    if(get_field('series_image', $queried_object)) {
                        $image = get_field('series_image', $queried_object);
                        if( !empty($image) ): 
                            // vars
                            $url = $image['url'];
                            $title = $image['title'];
                            // thumbnail
                            $size = 'blog-large';
                            $thumb = $image['sizes'][ $size ];
                            $width = $image['sizes'][ $size . '-width' ];
                            $height = $image['sizes'][ $size . '-height' ];
                    endif; ?>
                    <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                    <?php } ?>

        <?php if($description) { ?>
            <?php echo $description; ?>
            <hr />

    <?php } else { ?>                        
            <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>

        <?php } ?>


        <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
            <section class="entry-content cf">
                <?php if ( has_post_thumbnail() ) { 
                        $trim_number = 25;
                ?>
                <figure class="blog-thumb">                                
                    <?php the_post_thumbnail( 'blog-thumb' ); ?>                            
                </figure>                         
                <div class="details">                             
                <?php }else { 
                        $trim_number = 50;
                ?>
                <div class="details-wide">
                <?php } ?>
                    <?php if(get_the_term_list( $post->ID, 'careers_cat')){ ?>
                            <span class="cat-title">
                                <?php echo get_the_term_list( $post->ID, 'careers_cat', '', ' | ' , ''); ?>                                        
                            </span>
                        <?php } ?>
                    <?php if(get_field('date_of_event', $p->ID)) { ?>
                            <span class="date">
                                <h3><?php the_field('date_of_event', $p->ID); ?></h3>
                            </span>
                            <?php } ?>
                    <h4 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                    <?php if(get_field('time_of_event')) { ?>
                        <span class="time">
                           <?php the_field('time_of_event'); ?> 
                        </span>
                    <?php } ?>
                    <?php if(get_field('location')) { ?>
                            <span class="location">
                                @ <?php the_field('location'); ?>
                            </span>
                            <?php } ?>
                        <p>
                            <?php
                                $content = get_the_content();
                                $trimmed_content = wp_trim_words( $content, $trim_number, '...' );
                                echo $trimmed_content;
                            ?>
                        </p>
                    <a href="<?php the_permalink() ?>" class="btn">More Details</a>
                </div>
            </section>
        </article>
        <?php endwhile; ?>
            <?php else : ?>
            <h6 style="background: #efefef; padding: 10px; color: #4d4d4d; border-radius: 10px; font-size: 1em;">There were no results found.</h6>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</div>