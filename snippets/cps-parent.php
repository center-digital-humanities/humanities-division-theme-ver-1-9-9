<div class="home">
    
<?php

$post_object = get_field('page');

if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>
    <div class="col page-col">
    	<h3>Mission Statement<?php //the_title(); ?></h3>
        <p>
            <?php $content = get_the_content();
                $limit = get_field('word_limit');
                $trimmed_content = wp_trim_words( $content, $limit, '...' );
                echo $trimmed_content; 
            ?>
        </p>
        <a href="<?php the_permalink(); ?>" class="btn">Read More ></a>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>


<?                    
    $display = get_field('display');
    if( in_array('letter', $display) ) { 
        $letter = 'yes';
    } 
    if( in_array('interviews', $display) ) {
        $interviews = 'yes';
    } 
    if( in_array('qa', $display) ) {
        $qa = 'yes';
    }
    if( in_array('events', $display) ) {
        $events = 'yes';
    }
    if( in_array('awards', $display) ) {
        $awards = 'yes';
    }
    if( in_array('memoriam', $display) ) {
        $memoriam = 'yes';
    }
    if( in_array('arrivals', $display) ) {
        $arrivals = 'yes';
    }
 ?>

                
        <?php //$page = get_field('select_pages');

            $_SESSION['select_pages'] = $page;
?>

                <dl>
                    <?php 
                    $content = get_the_content();

                if($content){?>
                    
                <?php if($letter == 'yes') { ?>
                    <dd class="news-item">
                        <p>
                            <?php 
                            $trimmed_content = wp_trim_words( $content, 100, '...' );
                            echo $trimmed_content; ?>
                        </p>                        
                    </dd>                    
                <?php } } ?>
                    
                </dl>                
                <?php $youtube_id = get_field('youtube_video');
                    
                    if($youtube_id){
                ?>
                <div class="col youtube-col">
                <section name="youtube-video">
                    <h3>Video</h3>
                    <iframe src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>                
                </section>
                </div>
                <?php } ?>
                
                <?php

                                /* THIS WILL DISPLAY EVENTS BLOG */
                              // if(get_field('page_type') == "event-blog") {

                                     $_SESSION['page_type'] = get_field('page_type');
                                    //$term = get_sub_field('category');
                                    //$amount = get_sub_field('amount_to_show');
                                    $term = 'interviews';
                                    $amount = 10;

                                    // find date time now
                                    $date_now = date('Y-m-d H:i:s');
                                    $yesterday = date('Y-m-d H:i:s', strtotime("-1 day"));
                                    $time_now = strtotime($date_now);


                                    // find date time in 7 days
                                    $time_next_week = strtotime('+7 month', $time_now);
                                    $date_next_week = date('Y-m-d H:i:s', $time_next_week);


                                    // Upcoming Events Query (
                                    // query events
                                    $args = array(
                                            'careers_cat' => 'events, hcps', 
                                            'post_type' => 'careers', 
                                            'posts_per_page' => '2',
                                            'meta_query' 		=> array(
                                                array(
                                                    'key'			=> 'date_of_event',
                                                    'compare'		=> 'BETWEEN',
                                                    'value'			=> array( $yesterday, $date_next_week ),
                                                    'type'			=> 'DATETIME'
                                                )
                                            ),
                                            'order'				=> 'ASC',
                                            'orderby'			=> 'meta_value',
                                            'meta_key'			=> 'date_of_event',
                                            'meta_type'			=> 'DATETIME'
                                    );

                                   $posts_query = new WP_Query($args ); 

                                ?>
                                <div class="col events-col">
                                    <h3>Upcoming Events</h3>
                                    <ul class="blog">
                                        <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>

                                            <li>
                                        <?php if($description) { ?>
                                            <?php echo $description; ?>
                                            <hr />

                                    <?php } else { ?>                        
                                            <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>

                                        <?php } ?>
                  

                                        <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
                                            <section class="entry-content cf">
                                                <?php 
                                                        $trim_number = 9;
                                                ?>                              
                                                <?php the_post_thumbnail( 'people-thumb' ); ?>                         
                                                <div class="news-item">
                                                    <h4 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                                                    <div class="duration">
                                                    <?php if(get_field('date_of_event')) { ?>
                                                        <span class="date">
                                                            <?php the_field('date_of_event'); ?>
                                                        </span>
                                                    <?php } ?>
                                                    <?php if(get_field('time_of_event')) { ?>
                                                        <span class="time">
                                                           <?php the_field('time_of_event'); ?> 
                                                        </span>
                                                    <?php } ?>
                                                    <?php if(get_field('location')) { ?>
                                                            <span class="location">
                                                                @ <?php the_field('location'); ?>
                                                            </span>
                                                            <?php } ?>
                                                    </div>
                                                    <?php if( get_field('display_uevents') == 'display_descript' ){ 
                                                        echo 'YO';
                                                    ?>
                                                        <p>
                                                            <?php
                                                                $content = get_the_content();
                                                                $trimmed_content = wp_trim_words( $content, $trim_number, '...' );
                                                                echo $trimmed_content;
                                                            ?>
                                                        </p>
                                                    <?php } ?>
                                                </div>
                                            </section>
                                        </article>
                                            </li>
                                        <?php endwhile; ?>
                                        <?php else : ?>
                                        <h6 style="color: #4d4d4d; font-size: .85em;">There are no upcoming events.</h6>                                  
						              <!--a class="btn" href="/careers/hcps/">View All</a-->
                                    <?php endif; ?>
                                    </ul> 
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            <?php //} ?>

            <?php //endforeach; ?>
            <?php wp_reset_postdata(); ?>
            <?php //endif; ?>
</div>