<?php
            $date_of_event = get_field('date_of_event');
            $event_time = get_field('time_of_event');
            $event_location = get_field('location');
         ?>


    <div class="brief-intro">
        <span class="interview-photo">
            <?php if(get_sub_field('series_header_image')) {
            $image = get_sub_field('series_header_image');
            if( !empty($image) ): 
            // vars
            $url = $image['url'];
            $title = $image['title'];
            // thumbnail
            $size = 'content-width';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ];
        endif; ?>
        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo alignleft wp-post-image <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
        <?php } ?>
        </span>
        
        
        <div class="details">
            <dl>
                <?php if(get_field('first_name')) { ?>
                    <dt class="name">
                        <h3><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></h3>
                    </dt>
                <?php } ?>
                <?php if($date_of_event) { ?>
                <span class="date">
                    <strong>Date: </strong><?php echo $date_of_event; ?> | 
                </span>
                <?php } ?>
                <?php if($event_time) { ?>
                <span class="time">
                    <strong>Time: </strong><?php echo $event_time; ?> |  
                </span>
                <?php } ?>
                <?php if($event_location) { ?>
                <span class="location">
                    <strong>Location: </strong><?php echo $event_location; ?>
                </span>
                <?php } ?>

            </dl>									
        </div>                             

    </div>



<section class="thecontent">
    <?php the_content(); ?>
</section>
<section class="events">
    <?php session_start();
        if( have_rows('events_section') ) {
        while ( have_rows('events_section') ) { the_row();
                // This is for the Home blog page                     
                if(($_SESSION['select_pages']) || ($_SESSION['page_type'] == "interview-blog") ){ 
                    $i++;
                    if( $i > 2 ):
                        break; 
                    endif; 

                }
                                          
      
        // For showing snippet from any page
    //    if( get_row_layout() == 'event_details_block' ) {
            ?>


        
    <?php

    // For showing question and answers
   // } 
                                               
    if( get_row_layout() == 'speaker_block' ) {   
        echo '<section class="speakers">';
        echo '<h3>Speakers</h3>';
        
        // For displaying snippet to show Speakers list                              
        if( have_rows('the_speaker') ) {
            while ( have_rows('the_speaker') ) { the_row();
                                                              
        $speaker_name = get_sub_field('speaker_name');
        $speaker_bio = get_sub_field('speaker_bio'); ?>
        <dl class="speakers">
            <span class="speaker-photo">
                <?php if(get_sub_field('speaker_photo')) {
                $image = get_sub_field('speaker_photo');
                if( !empty($image) ): 
                // vars
                $url = $image['url'];
                $title = $image['title'];
                // thumbnail
                $size = 'people-thumb';
                $thumb = $image['sizes'][ $size ];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ];
            endif; ?>
            <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo alignleft wp-post-image <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
            <?php } ?>
            </span>
            <dt class="name">            
                <?php if ($speaker_name){   echo '<h4>' . $speaker_name . '</h4>';  } ?>
            </dt>
            <dd class="bio">
                <?php if ($speaker_bio){ echo $speaker_bio; } ?>
            </dd>
        </dl>
    <?php }} ?>
    </section>
            
            <?php

    // For showing quotes
    } elseif( get_row_layout() == 'moderator_block' ) {
        echo '<section class="moderator">';
        echo '<h3>Moderator</h3>';
        
         // For displaying snippet to show Moderator list                              
        if( have_rows('the_moderator') ) {
            while ( have_rows('the_moderator') ) { the_row();
            
        $moderator_name = get_sub_field('moderator_name');
        $moderator_bio = get_sub_field('moderator_bio'); ?>
        <dl class="moderator">
            <span class="moderator-photo">
                <?php if(get_sub_field('moderator_photo')) {
                $image = get_sub_field('moderator_photo');
                if( !empty($image) ): 
                // vars
                $url = $image['url'];
                $title = $image['title'];
                // thumbnail
                $size = 'people-thumb';
                $thumb = $image['sizes'][ $size ];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ];
            endif; ?>
            <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo alignleft wp-post-image <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
            <?php } ?>
            </span>
            <dt class="name">            
                <?php if ($moderator_name){   echo '<h4>' . $moderator_name . '</h4>';  } ?>
            </dt>
            <dd class="bio">
                <?php if ($moderator_bio){ echo $moderator_bio; } ?>
            </dd>
        </dl>
    
            <?php }} ?>
        </section>
    
         <section>
    <?php
        
         }
           elseif( get_row_layout() == 'add_event_flyer' ) {?>
            <a href="<?php the_sub_field('upload_flyer'); ?>" class="btn">Event Flyer</a>              
           <?php }
           elseif( get_row_layout() == 'add_event_form' ) { 
               $event_form_section = get_sub_field('event_form_section'); 
               if($event_form_section){
            ?>
            <section class="form">
                <?php echo $event_form_section; ?>
            </section>
               
               
           <?php } 
               }
                                               
        }
    }

        ?>

</section>